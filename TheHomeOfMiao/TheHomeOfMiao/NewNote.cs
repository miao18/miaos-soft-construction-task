﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheHomeOfMiao
{
    public partial class NewNote : Form
    {
        int studyId = 0;
        string checkGoal = "";
        string nameNote = "";
        public NewNote(int s,string c)
        {
            InitializeComponent();
            studyId = s;
            checkGoal = c;
            using (SQLiteConnection connection = new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter("select name from checkGoal", connection))
                {
                    DataSet ds = new DataSet();
                    adapter.Fill(ds, "checkGoal");
                    DataTable dtGroup = ds.Tables["checkGoal"];

                    check.DataSource = dtGroup;
                    check.DisplayMember = "name";
                    check.ValueMember = "name";
                    check.SelectedIndex = -1;
                }
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter("select name from notebook", connection))
                {
                    DataSet ds = new DataSet();
                    adapter.Fill(ds, "checkGoal");
                    DataTable dtGroup = ds.Tables["checkGoal"];

                    notebook.DataSource = dtGroup;
                    notebook.DisplayMember = "name";
                    notebook.ValueMember = "name";
                    notebook.SelectedIndex = -1;
                }
                connection.Close();
            }
        }

        private void button5_Click(object sender, EventArgs e)//创建
        {
            nameNote= this.name.Text.ToString();
            checkGoal = this.check.Text.ToString();
            string noteBookName = notebook.Text.ToString();
            if (nameNote == "") return;
            if (checkGoal != "" && checkGoal != noteBookName) return;
            if (noteBookName == ""&&checkGoal=="") { noteBookName = "漂流笔记本"; }
            using (SQLiteConnection connection=new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand("insert into note values ('" + nameNote + "'," + studyId + ",'" + noteBookName + "','')", connection))
                {
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            Thread th = new Thread(delegate () { new Note(nameNote).ShowDialog(); });
            th.Start();
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)//快建
        {
            string notebook;
            if (checkGoal != "")
            {
                notebook = checkGoal;
            }
            else
            {
                notebook = "漂流笔记本";
            }
            using (SQLiteConnection connection=new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteCommand command=new SQLiteCommand("insert into note values ('" + DateTime.Now.ToString() + "'," + studyId + ",'" + notebook + "','')", connection))
                {
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            Thread th = new Thread(delegate () { new Note(DateTime.Now.ToString()).ShowDialog(); });
            th.Start();
            this.Close();
        }

        
    }
}
