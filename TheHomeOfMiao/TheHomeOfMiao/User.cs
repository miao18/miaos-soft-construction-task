﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheHomeOfMiao
{
    public partial class User : Form
    {
        public User()
        {
            InitializeComponent();
            username.Text = Account.user.username;
            password.Text = Account.user.password;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(delegate () { new Main().ShowDialog(); });
            th.Start();
            this.Close();
        }

        

        private void button3_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(delegate () { new CheckRecord().ShowDialog(); });
            th.Start();
            this.Close();
        }

        

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("确认更改？");
            using (SQLiteConnection connection = new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand("update user set username='" + username.Text.ToString() + "',password='" + password.Text.ToString() + "' where username='" + Account.user.username + "'", connection))
                {
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            MessageBox.Show("保存成功");
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            Thread th = new Thread(delegate () { new 喵喵的小屋().ShowDialog(); });
            th.Start();
            this.Close();
        }
    }
}
