﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheHomeOfMiao
{
    public partial class StartStudy : Form
    {
        public static int count = 0;
        public StartStudy()
        {
            InitializeComponent();
            using (SQLiteConnection connection=new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteCommand command=new SQLiteCommand("select * from study",connection))
                {
                    SQLiteDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        count = 0;
                        while (reader.Read())
                        {
                            count++;
                        }
                    }
                    reader.Close();
                }
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter("select name from checkGoal", connection))
                {
                    DataSet ds = new DataSet();
                    adapter.Fill(ds, "checkGoal");
                    DataTable dtGroup = ds.Tables["checkGoal"];

                    comboBox1.DataSource = dtGroup;
                    comboBox1.DisplayMember = "name";
                    comboBox1.ValueMember = "name";
                    comboBox1.SelectedIndex = -1;
                }
                connection.Close();
            }
        }

        private void 快速自习_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection connection=new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                count++;
                using (SQLiteCommand command=new SQLiteCommand("insert into study ('type','date','time','checkGoal','id') values ('N','"+ DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "','"+time.Text.ToString()+"','','"+count+"')",connection))
                {
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            Study study = new Study("N",time.Text.ToString());
            Thread th = new Thread(delegate () { study.ShowDialog(); });
            th.Start();
            this.Close();
        }

        private void 自习_Click(object sender, EventArgs e)
        {
            string type="";
            string studyTime = time.Text.ToString();
            string goal="";
            if (this.typeY.Checked&& typeN.Checked)
            {
                MessageBox.Show("只能选一种模式");
                return;
            }
            if (this.typeY.Checked)
            {
                type = "Y";
            }
            if (typeN.Checked)
            {
                type = "N";
                studyTime = "0:0:0";
            }
            if (comboBox1.Text.ToString() != "") { goal = comboBox1.Text.ToString(); }
            if (type == ""||(type=="Y"&&studyTime=="0:0:0")) return;
            using (SQLiteConnection connection = new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                count++;
                using (SQLiteCommand command = new SQLiteCommand("insert into study ('type','date','time','checkGoal','id') values ('" + type+"','" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "','" + studyTime + "','"+goal+"','"+count+"')", connection))
                {
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            Study study = new Study(type, studyTime);
            Thread th = new Thread(delegate () { study.ShowDialog(); });
            th.Start();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(delegate () { new Main().ShowDialog(); });
            th.Start();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(delegate () { new StudyRecord().ShowDialog(); });
            th.Start();
            this.Close();
        }
    }
}
