﻿
namespace TheHomeOfMiao
{
    partial class StartStudy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.typeY = new System.Windows.Forms.CheckBox();
            this.typeN = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.time = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.快速自习 = new System.Windows.Forms.Button();
            this.自习 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("华文行楷", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(180, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 29);
            this.label1.TabIndex = 5;
            this.label1.Text = "开始自习";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("华文行楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(87, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 21);
            this.label2.TabIndex = 7;
            this.label2.Text = "类型";
            // 
            // typeY
            // 
            this.typeY.AutoSize = true;
            this.typeY.Location = new System.Drawing.Point(185, 76);
            this.typeY.Name = "typeY";
            this.typeY.Size = new System.Drawing.Size(59, 19);
            this.typeY.TabIndex = 8;
            this.typeY.Text = "定时";
            this.typeY.UseVisualStyleBackColor = true;
            // 
            // typeN
            // 
            this.typeN.AutoSize = true;
            this.typeN.Location = new System.Drawing.Point(276, 76);
            this.typeN.Name = "typeN";
            this.typeN.Size = new System.Drawing.Size(74, 19);
            this.typeN.TabIndex = 9;
            this.typeN.Text = "不限时";
            this.typeN.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("华文行楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(87, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 21);
            this.label3.TabIndex = 10;
            this.label3.Text = "时长";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("华文行楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(87, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 21);
            this.label4.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("华文行楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(87, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 21);
            this.label5.TabIndex = 12;
            this.label5.Text = "打卡目标";
            // 
            // time
            // 
            this.time.Location = new System.Drawing.Point(185, 125);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(218, 25);
            this.time.TabIndex = 26;
            this.time.Text = "0:0:0";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(185, 174);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(216, 23);
            this.comboBox1.TabIndex = 28;
            // 
            // 快速自习
            // 
            this.快速自习.BackColor = System.Drawing.Color.AntiqueWhite;
            this.快速自习.FlatAppearance.BorderSize = 0;
            this.快速自习.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SandyBrown;
            this.快速自习.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SandyBrown;
            this.快速自习.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.快速自习.Location = new System.Drawing.Point(91, 233);
            this.快速自习.Name = "快速自习";
            this.快速自习.Size = new System.Drawing.Size(113, 36);
            this.快速自习.TabIndex = 33;
            this.快速自习.Text = "快速自习";
            this.快速自习.UseVisualStyleBackColor = true;
            this.快速自习.Click += new System.EventHandler(this.快速自习_Click);
            // 
            // 自习
            // 
            this.自习.BackColor = System.Drawing.Color.AntiqueWhite;
            this.自习.FlatAppearance.BorderSize = 0;
            this.自习.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SandyBrown;
            this.自习.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SandyBrown;
            this.自习.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.自习.Location = new System.Drawing.Point(288, 233);
            this.自习.Name = "自习";
            this.自习.Size = new System.Drawing.Size(113, 36);
            this.自习.TabIndex = 34;
            this.自习.Text = "自习";
            this.自习.UseVisualStyleBackColor = true;
            this.自习.Click += new System.EventHandler(this.自习_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.AntiqueWhite;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SandyBrown;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SandyBrown;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(12, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(59, 42);
            this.button1.TabIndex = 35;
            this.button1.Text = "<";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.AntiqueWhite;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SandyBrown;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SandyBrown;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(375, 13);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(113, 36);
            this.button2.TabIndex = 36;
            this.button2.Text = "自习记录";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // StartStudy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.ClientSize = new System.Drawing.Size(500, 302);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.自习);
            this.Controls.Add(this.快速自习);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.time);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.typeN);
            this.Controls.Add(this.typeY);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "StartStudy";
            this.Text = "StartStudy";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox typeY;
        private System.Windows.Forms.CheckBox typeN;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox time;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button 快速自习;
        private System.Windows.Forms.Button 自习;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}