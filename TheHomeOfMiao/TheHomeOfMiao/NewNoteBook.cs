﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheHomeOfMiao
{
    public partial class NewNoteBook : Form
    {
        public NewNoteBook()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (bookname.Text.ToString() == "") return;
            using (SQLiteConnection connection=new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3")) {
                connection.Open();
                Random random = new Random();
                int image = random.Next(1, 5);
                using (SQLiteCommand command=new SQLiteCommand("insert into notebook values ('"+bookname.Text.ToString()+"','D','C','"+image+"')",connection))
                {
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            Thread th = new Thread(delegate () { new NoteBook().ShowDialog(); });
            th.Start();
            this.Close();
        }
    }
}
