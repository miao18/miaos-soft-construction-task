﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheHomeOfMiao
{
    public partial class CheckRecord : Form
    {
        public CheckRecord()
        {
            InitializeComponent();
            using (SQLiteConnection connection = new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteCommand command1 = new SQLiteCommand("select * from checkGoal where user='" + Account.user.username + "' and state='N'", connection))
                {
                    SQLiteDataReader reader = command1.ExecuteReader();
                    for (int i = 1; reader.Read(); i++)
                    {
                        Button btn = new Button();//声明 实例控件 
                        btn.Name = reader["name"].ToString(); //控件名称
                        btn.BackColor = Color.Transparent;
                        bool isCheck = false;
                        using (SQLiteCommand command = new SQLiteCommand("select * from checkRecord where state='Y'", connection))
                        {
                            SQLiteDataReader readerCheck = command.ExecuteReader();
                            while (readerCheck.Read())
                            {
                                if (readerCheck["checkGoal"].ToString() == reader["name"].ToString())
                                {
                                    isCheck = true;
                                    btn.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\选中图标.png");
                                }
                            }
                            readerCheck.Close();
                        }
                        if (!isCheck) { btn.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\图标" + reader["image"].ToString() + ".png"); }
                        btn.BackgroundImageLayout = ImageLayout.Stretch;
                        btn.FlatAppearance.BorderSize = 0;
                        btn.FlatAppearance.MouseDownBackColor = Color.Transparent;
                        btn.FlatAppearance.MouseOverBackColor = Color.Transparent;
                        btn.FlatStyle = FlatStyle.Flat;
                        btn.ForeColor = Color.MistyRose;
                        btn.Location = new Point(40 + ((i - 1) % 2) * 150, 25 + (i - 1) / 2 * 135);
                        btn.Size = new Size(80, 70);
                        btn.Click += new EventHandler(btn_Click);//定义单机事件
                        //btn.MouseDown += new MouseEventHandler(btn_MouseDown);
                        Label name = new Label();
                        name.Name = "打卡" + i;
                        name.Text = reader["name"].ToString();
                        name.Location = new Point(65 + ((i - 1) % 2) * 150, 100 + (i - 1) / 2 * 135);
                        this.panel1.Controls.Add(btn);//将控件添加到窗体或容器
                        this.panel1.Controls.Add(name);
                    }
                    reader.Close();
                }
                connection.Close();
            }
        }
        private void btn_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection connection = new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                int total; int days = 0;
                string[] start; string[] end;
                using (SQLiteCommand command = new SQLiteCommand("select * from checkGoal where name='" + ((Button)sender).Name.ToString() + "' and state='N'", connection))
                {
                    SQLiteDataReader reader = command.ExecuteReader();
                    reader.Read();
                    string startTime = reader["startTime"].ToString();
                    string endTime = reader["endTime"].ToString();
                    start = startTime.Split('/');
                    end = endTime.Split('/');
                    total = (int.Parse(end[0]) * 365 + int.Parse(end[1]) * 30 + int.Parse(end[2])) - (int.Parse(start[0]) * 365 + int.Parse(start[1]) * 30 + int.Parse(start[2]));
                    reader.Close();
                }
                using (SQLiteCommand command1 = new SQLiteCommand("select * from checkRecord where checkGoal='" + ((Button)sender).Name.ToString() + "'", connection))
                {
                    SQLiteDataReader reader1 = command1.ExecuteReader();
                    while (reader1.Read())
                    {
                        string checkTime = reader1["checkTime"].ToString();
                        string[] check = checkTime.Split('/');
                        if ((int.Parse(check[0]) * 365 + int.Parse(check[1]) * 30 + int.Parse(check[2])) >= (int.Parse(start[0]) * 365 + int.Parse(start[1]) * 30 + int.Parse(start[2])) && (int.Parse(check[0]) * 365 + int.Parse(check[1]) * 30 + int.Parse(check[2])) <= (int.Parse(end[0]) * 365 + int.Parse(end[1]) * 30 + int.Parse(end[2])))
                        {
                            days++;
                        }
                    }
                    reader1.Close();
                }
                connection.Close();
                detail.Text="应打卡:\n" + total + "天\n" + "已打卡:\n" + days + "天";
            }
        }

            private void button1_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(delegate () { new Check().ShowDialog(); });
            th.Start();
            this.Close();
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void detail_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
