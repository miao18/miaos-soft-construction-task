﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheHomeOfMiao
{
    public partial class Note : Form
    {
        public Note(string n)
        {
            InitializeComponent();
            name.Text = n;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(delegate () { new NoteBook().ShowDialog(); });
            th.Start();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection connection=new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteCommand command=new SQLiteCommand("update note set content='"+笔记.Text.ToString()+"' where name='"+name.Text.ToString()+"'",connection))
                {
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }

        public void setNote()
        {
            using (SQLiteConnection connection=new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteCommand command=new SQLiteCommand("select * from note where name='"+name.Text.ToString()+"'",connection))
                {
                    SQLiteDataReader reader = command.ExecuteReader();
                    reader.Read();
                    笔记.Text = reader["content"].ToString();
                    reader.Close();
                }
                connection.Close();
            }
        }

        
    }
}
