﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Threading;

namespace TheHomeOfMiao
{
    public partial class 喵喵的小屋 : Form
    {
        public 喵喵的小屋()
        {
            InitializeComponent();
            using (SQLiteConnection connection=new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteCommand command=new SQLiteCommand("select * from checkGoal", connection))
                {
                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        string endTime = reader["endTime"].ToString();
                        string[] end = endTime.Split('/');
                        string nowTime = DateTime.Now.ToShortDateString();
                        string[] now = nowTime.Split('/');
                        if ((int.Parse(now[0]) * 365 + int.Parse(now[1]) * 30 + int.Parse(now[2])) > (int.Parse(end[0]) * 365 + int.Parse(end[1]) * 30 + int.Parse(end[2])))
                        {
                            using (SQLiteCommand command1=new SQLiteCommand("update checkGoal set state='N' where name='" + reader["name"] + "'",connection))
                            {
                                command1.ExecuteNonQuery();
                            }
                            using (SQLiteCommand command1 = new SQLiteCommand("update checkRecord set goalState='N' where checkGoal='" + reader["name"] + "'",connection))
                            {
                                command1.ExecuteNonQuery();
                            }
                            using (SQLiteCommand command1 = new SQLiteCommand("update notebook set State='N' where name='" + reader["name"] + "'", connection))
                            {
                                command1.ExecuteNonQuery();
                            }
                        }
                    }
                    reader.Close();
                }
                using (SQLiteCommand command = new SQLiteCommand("select * from checkRecord", connection))
                {
                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        string checkTime = reader["checkTime"].ToString();
                        string[] check = checkTime.Split('/');
                        string nowTime = DateTime.Now.ToShortDateString();
                        string[] now = nowTime.Split('/');
                        if ((int.Parse(now[0]) * 365 + int.Parse(now[1]) * 30 + int.Parse(now[2])) > (int.Parse(check[0]) * 365 + int.Parse(check[1]) * 30 + int.Parse(check[2])))
                        {
                            using (SQLiteCommand command1 = new SQLiteCommand("update checkRecord set state='N' where checkGoal='" + reader["checkGoal"] + "' and checkTime='"+checkTime+"'",connection))
                            {
                                command1.ExecuteNonQuery();
                            }
                        }
                    }
                    reader.Close();
                }
                connection.Close();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string username = this.username.Text.ToString();
            string password = this.password.Text.ToString();
            if (username == "" || password == "") return;
            using (SQLiteConnection connection = new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand("select * from user ", connection))
                {
                    SQLiteDataReader reader = command.ExecuteReader();
                    bool isExist = false;
                    while (reader.Read())
                    {
                        if (reader["username"].ToString() == username)
                        {
                            isExist = true;
                            if (reader["password"].ToString() == password)
                            {
                                Account a = new Account(username, password);
                                Account.user = a;
                                //打开另一个窗口的同时关闭当前窗口
                                Thread th = new Thread(delegate () { new Main().ShowDialog(); });
                                th.Start();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("密码错误");
                            }
                        }
                    }
                    reader.Close();
                    if (isExist) { connection.Close(); return; }
                }
                   
                
                if (MessageBox.Show("你想要创建一个新的账户吗？新账户将不与显示原账户学习记录。", "创建新账户", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    Account a = new Account(username, password);
                    Account.user = a;
                    using (SQLiteCommand command2 = new SQLiteCommand("insert into user (username, password) values ('" + username + "', '" + password + "')", connection))
                    {
                        command2.ExecuteNonQuery();
                    }
                        
                }

                connection.Close();
            }
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ForgetPassword f = new ForgetPassword();
            f.Show();
        }
    }
}
