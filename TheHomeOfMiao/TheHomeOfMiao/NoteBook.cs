﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheHomeOfMiao
{
    public partial class NoteBook : Form
    {
        public NoteBook()
        {
            InitializeComponent();
            using (SQLiteConnection connection = new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteCommand command1 = new SQLiteCommand("select * from notebook", connection))
                {
                    SQLiteDataReader reader = command1.ExecuteReader();
                    for (int i = 1; reader.Read(); i++)
                    {
                        Button btn = new Button();//声明 实例控件 
                        btn.Name = reader["name"].ToString(); //控件名称
                        btn.BackColor = Color.Transparent;
                        btn.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\图标" + reader["image"].ToString() + ".png"); 
                        btn.BackgroundImageLayout = ImageLayout.Stretch;
                        btn.FlatAppearance.BorderSize = 0;
                        btn.FlatAppearance.MouseDownBackColor = Color.Transparent;
                        btn.FlatAppearance.MouseOverBackColor = Color.Transparent;
                        btn.FlatStyle = FlatStyle.Flat;
                        btn.ForeColor = Color.MistyRose;
                        btn.Location = new Point(40 + ((i - 1) % 2) * 150, 25 + (i - 1) / 2 * 135);
                        btn.Size = new Size(80, 70);
                        btn.Click += new EventHandler(btn_Click);//定义单机事件
                        //btn.MouseDown += new MouseEventHandler(btn_MouseDown);
                        Label name = new Label();
                        name.Name = "笔记" + i;
                        name.Text = reader["name"].ToString();
                        name.Location = new Point(65 + ((i - 1) % 2) * 150, 100 + (i - 1) / 2 * 135);
                        this.panel1.Controls.Add(btn);//将控件添加到窗体或容器
                        this.panel1.Controls.Add(name);
                    }
                    reader.Close();
                }
                connection.Close();
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(delegate () { new Notes(((Button)sender).Name.ToString()).ShowDialog(); });
            th.Start();
            this.Close();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(delegate () { new NewNote(0,"").ShowDialog(); });
            th.Start();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(delegate () { new NewNoteBook().ShowDialog(); });
            th.Start();
            this.Close();
        }
    }
}
