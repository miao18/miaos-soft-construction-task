﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace TheHomeOfMiao
{
    public partial class SetRest : Form
    {
        System.Timers.Timer aTimer = new System.Timers.Timer();
        public SetRest()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (restTime.Text.ToString() == "") return;
            switch (this.restTime.Text.ToString())
            {
                case "5分钟": this.Time.Text = "5:0";break;
                case "10分钟": this.Time.Text = "10:0"; break;
                case "20分钟": this.Time.Text = "20:0"; break;
            }
            aTimer.Elapsed += new ElapsedEventHandler(subPollingEvent); //到达时间的时候执行事件；
            aTimer.Interval = 1000; // 设置引发时间的时间间隔 此处设置为1秒（1000毫秒）
            aTimer.AutoReset = true;//设置是执行一次（false）还是一直执行(true)；
            aTimer.Enabled = true; //是否执行System.Timers.Timer.Elapsed事件；
            aTimer.Start();
        }
        private void subPollingEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            subTime();
        }

        private void subTime()
        {
            if (Time.Text.ToString() == "0:0")
            {
                MessageBox.Show("休息结束，请返回自习");
                return;
            }
            //创建一个委托，用于封装一个方法，在这里是封装了 控制更新控件 的方法
            Action invokeAction = new Action(subTime);
            //判断操作控件的线程是否创建控件的线程
            //调用方调用方位于创建控件所在的线程以外的线程中，如果在其他线程则对控件进行方法调用时必须调用 Invoke 方法
            if (this.InvokeRequired)
            {
                //与调用线程不同的线程上创建（说明您必须通过 Invoke 方法对控件进行调用）
                this.Invoke(invokeAction);
            }
            else
            {
                //窗体线程，即主线程
                string[] t = Time.Text.ToString().Split(':');
                int minute = int.Parse(t[0]), second = int.Parse(t[1]);
                if (second == 0)
                {
                    second = 60;
                    minute--;
                }
                second--;
                string setTime = "";
                setTime += minute;
                setTime += ":";
                setTime += second;
                Time.Text = setTime;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            aTimer.Stop();
            Study.aTimer.Start();
            this.Close();
        }
    }
}
