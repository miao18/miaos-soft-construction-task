﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheHomeOfMiao
{
    public partial class Main : Form
    {
        
        public Main()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(delegate () { new Check().ShowDialog(); });
            th.Start();
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(delegate () { new StartStudy().ShowDialog(); });
            th.Start();
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            new NoteBook().ShowDialog(); 
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(delegate () { new User().ShowDialog(); });
            th.Start();
            this.Close();
        }
    }
}
