﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Threading;

namespace TheHomeOfMiao
{
    public partial class NewCheckGoal : Form
    {
        static int imageIndex;
        public NewCheckGoal()
        {
            InitializeComponent();
            imageIndex = 0;
        }

        private void 创建打卡_Click(object sender, EventArgs e)
        {
            string name = this.name.Text.ToString();
            string[] start = this.startTime.Value.ToShortDateString().Split('/');
            string[] end = this.endTime.Value.ToShortDateString().Split('/');
            string user = Account.user.username;
            string state = "Y";
            if (name == "" || this.startTime.Value.ToShortDateString() == "" || this.endTime.Value.ToShortDateString() == "") return;
            int startDate =int.Parse(start[0])*365+int.Parse(start[1])*30+int.Parse(start[2]);
            int endDate = int.Parse(end[0]) * 365 + int.Parse(end[1]) * 30 + int.Parse(end[2]);
            if (startDate>endDate)
            {
                MessageBox.Show("结束日期不能小于开始日期。");
                return;
            }
            if (imageIndex == 0)
            {
                MessageBox.Show("选一个图标吧。");
                return;
            }
            using (SQLiteConnection connection = new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteCommand command1 = new SQLiteCommand("select * from checkGoal where name='" + name + "'", connection))
                {
                    SQLiteDataReader reader = command1.ExecuteReader();
                    while (reader.Read())
                    {
                        string[] endG = reader["endTime"].ToString().Split('/');
                        int endDateG = int.Parse(endG[0]) * 365 + int.Parse(endG[1]) * 30 + int.Parse(endG[2]);
                        if (reader["state"].ToString() == "Y" && (startDate<=endDateG))
                        {
                            MessageBox.Show("已存在未结束打卡目标。");
                            connection.Close();
                            return;
                        }
                    }
                    reader.Close();
                }
                using (SQLiteCommand command2 = new SQLiteCommand("insert into checkGoal values ('" + name + "','" + startTime.Value.ToShortDateString() + "','" + endTime.Value.ToShortDateString() + "','" + user + "','" + state + "'," + imageIndex + ")", connection))
                {
                    command2.ExecuteNonQuery();
                }
                using (SQLiteCommand command3=new SQLiteCommand("insert into notebook values ('"+name+"','C','Y','"+imageIndex+"')", connection))
                {
                    command3.ExecuteNonQuery();
                }
                MessageBox.Show("创建成功。");
                connection.Close();
            }
            Thread th = new Thread(delegate () { new Check().ShowDialog(); });
            th.Start();
            this.Close();
        }
        
        private void 图标1_Click(object sender, EventArgs e)
        {
            imageIndex = 1;
            this.图标1.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\选中图标.png");
            this.图标2.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\图标2.png");
            this.图标3.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\图标3.png");
            this.图标4.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\图标4.png");
        }

        private void 图标2_Click(object sender, EventArgs e)
        {
            imageIndex = 2;
            this.图标2.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\选中图标.png");
            this.图标1.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\图标1.png");
            this.图标3.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\图标3.png");
            this.图标4.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\图标4.png");
        }

        private void 图标3_Click(object sender, EventArgs e)
        {
            imageIndex = 3;
            this.图标3.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\选中图标.png");
            this.图标2.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\图标2.png");
            this.图标1.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\图标1.png");
            this.图标4.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\图标4.png");
        }

        private void 图标4_Click(object sender, EventArgs e)
        {
            imageIndex = 4;
            this.图标4.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\选中图标.png");
            this.图标2.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\图标2.png");
            this.图标3.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\图标3.png");
            this.图标1.BackgroundImage = new Bitmap(@"D:\TheHomeOfMiao\TheHomeOfMiao\Resources\图标1.png");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(delegate () { new Check().ShowDialog(); });
            th.Start();
            this.Close();
        }
    }
}
