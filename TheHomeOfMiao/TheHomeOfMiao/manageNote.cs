﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace TheHomeOfMiao
{
    public partial class manageNote : Form
    {
        string name;
        public manageNote(string n)
        {
            InitializeComponent();
            name = n;
            rename.Text = n;
            using (SQLiteConnection connection = new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter("select name from notebook", connection))
                {
                    DataSet ds = new DataSet();
                    adapter.Fill(ds, "checkGoal");
                    DataTable dtGroup = ds.Tables["checkGoal"];

                    comboBox1.DataSource = dtGroup;
                    comboBox1.DisplayMember = "name";
                    comboBox1.ValueMember = "name";
                    comboBox1.SelectedIndex = -1;
                }
                connection.Close();
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection connection = new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand("update note set notebook='"+comboBox1.Text.ToString()+"' where name='"+name+"'", connection))
                {
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            MessageBox.Show("移动成功");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection connection = new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand("delete from note where name='" + name + "'", connection))
                {
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            MessageBox.Show("删除成功");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection connection = new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand("update note set name='"+rename.Text.ToString()+"' where name='"+name+"'", connection))
                {
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            MessageBox.Show("重命名成功");
        }
    }
}
