﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheHomeOfMiao
{
    public partial class StudyRecord : Form
    {
        public StudyRecord()
        {
            InitializeComponent();
            using (SQLiteConnection connection=new SQLiteConnection(@"DataSource=..\..\Resources\TheHomeOfMiao1.db;Version=3"))
            {
                connection.Open();
                using (SQLiteDataAdapter adapter=new SQLiteDataAdapter("select date as 日期 ,time as 时长 ,checkGoal as 打卡目标 from study",connection))
                {
                    DataTable mTable = new DataTable(); // Don't forget initialize!
                    adapter.Fill(mTable);

                    // 绑定数据到DataGridView
                    studys.DataSource = mTable;
                }
                connection.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(delegate () { new StartStudy().ShowDialog(); });
            th.Start();
            this.Close();
        }
    }
}
