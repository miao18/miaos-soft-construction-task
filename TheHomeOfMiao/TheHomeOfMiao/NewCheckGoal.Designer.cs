﻿
namespace TheHomeOfMiao
{
    partial class NewCheckGoal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewCheckGoal));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.图标1 = new System.Windows.Forms.Button();
            this.图标2 = new System.Windows.Forms.Button();
            this.图标3 = new System.Windows.Forms.Button();
            this.图标4 = new System.Windows.Forms.Button();
            this.name = new System.Windows.Forms.TextBox();
            this.创建打卡 = new System.Windows.Forms.Button();
            this.startTime = new System.Windows.Forms.DateTimePicker();
            this.endTime = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("华文行楷", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(154, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 29);
            this.label1.TabIndex = 3;
            this.label1.Text = "新建打卡目标";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("华文行楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(80, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 21);
            this.label2.TabIndex = 5;
            this.label2.Text = "名称";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("华文行楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(80, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 21);
            this.label3.TabIndex = 6;
            this.label3.Text = "开始日期";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("华文行楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(80, 182);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 21);
            this.label4.TabIndex = 7;
            this.label4.Text = "结束日期";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("华文行楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(80, 239);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 21);
            this.label5.TabIndex = 8;
            this.label5.Text = "图标：";
            // 
            // 图标1
            // 
            this.图标1.BackColor = System.Drawing.Color.Transparent;
            this.图标1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("图标1.BackgroundImage")));
            this.图标1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.图标1.FlatAppearance.BorderSize = 0;
            this.图标1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.图标1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.图标1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.图标1.ForeColor = System.Drawing.Color.MistyRose;
            this.图标1.Location = new System.Drawing.Point(84, 287);
            this.图标1.Name = "图标1";
            this.图标1.Size = new System.Drawing.Size(66, 60);
            this.图标1.TabIndex = 21;
            this.图标1.UseVisualStyleBackColor = false;
            this.图标1.Click += new System.EventHandler(this.图标1_Click);
            // 
            // 图标2
            // 
            this.图标2.BackColor = System.Drawing.Color.Transparent;
            this.图标2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("图标2.BackgroundImage")));
            this.图标2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.图标2.FlatAppearance.BorderSize = 0;
            this.图标2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.图标2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.图标2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.图标2.ForeColor = System.Drawing.Color.MistyRose;
            this.图标2.Location = new System.Drawing.Point(168, 287);
            this.图标2.Name = "图标2";
            this.图标2.Size = new System.Drawing.Size(66, 60);
            this.图标2.TabIndex = 22;
            this.图标2.UseVisualStyleBackColor = false;
            this.图标2.Click += new System.EventHandler(this.图标2_Click);
            // 
            // 图标3
            // 
            this.图标3.BackColor = System.Drawing.Color.Transparent;
            this.图标3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("图标3.BackgroundImage")));
            this.图标3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.图标3.FlatAppearance.BorderSize = 0;
            this.图标3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.图标3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.图标3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.图标3.ForeColor = System.Drawing.Color.MistyRose;
            this.图标3.Location = new System.Drawing.Point(249, 287);
            this.图标3.Name = "图标3";
            this.图标3.Size = new System.Drawing.Size(66, 60);
            this.图标3.TabIndex = 23;
            this.图标3.UseVisualStyleBackColor = false;
            this.图标3.Click += new System.EventHandler(this.图标3_Click);
            // 
            // 图标4
            // 
            this.图标4.BackColor = System.Drawing.Color.Transparent;
            this.图标4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("图标4.BackgroundImage")));
            this.图标4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.图标4.FlatAppearance.BorderSize = 0;
            this.图标4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.图标4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.图标4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.图标4.ForeColor = System.Drawing.Color.MistyRose;
            this.图标4.Location = new System.Drawing.Point(340, 287);
            this.图标4.Name = "图标4";
            this.图标4.Size = new System.Drawing.Size(66, 60);
            this.图标4.TabIndex = 24;
            this.图标4.UseVisualStyleBackColor = false;
            this.图标4.Click += new System.EventHandler(this.图标4_Click);
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(188, 70);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(218, 25);
            this.name.TabIndex = 25;
            // 
            // 创建打卡
            // 
            this.创建打卡.BackColor = System.Drawing.Color.AntiqueWhite;
            this.创建打卡.FlatAppearance.BorderSize = 0;
            this.创建打卡.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SandyBrown;
            this.创建打卡.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SandyBrown;
            this.创建打卡.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.创建打卡.Location = new System.Drawing.Point(188, 378);
            this.创建打卡.Name = "创建打卡";
            this.创建打卡.Size = new System.Drawing.Size(108, 40);
            this.创建打卡.TabIndex = 28;
            this.创建打卡.Text = "创建";
            this.创建打卡.UseVisualStyleBackColor = true;
            this.创建打卡.Click += new System.EventHandler(this.创建打卡_Click);
            // 
            // startTime
            // 
            this.startTime.Location = new System.Drawing.Point(188, 128);
            this.startTime.Name = "startTime";
            this.startTime.Size = new System.Drawing.Size(217, 25);
            this.startTime.TabIndex = 29;
            // 
            // endTime
            // 
            this.endTime.Location = new System.Drawing.Point(188, 182);
            this.endTime.Name = "endTime";
            this.endTime.Size = new System.Drawing.Size(217, 25);
            this.endTime.TabIndex = 30;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.AntiqueWhite;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SandyBrown;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SandyBrown;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(59, 42);
            this.button1.TabIndex = 31;
            this.button1.Text = "<";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // NewCheckGoal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.ClientSize = new System.Drawing.Size(500, 444);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.endTime);
            this.Controls.Add(this.startTime);
            this.Controls.Add(this.创建打卡);
            this.Controls.Add(this.name);
            this.Controls.Add(this.图标4);
            this.Controls.Add(this.图标3);
            this.Controls.Add(this.图标2);
            this.Controls.Add(this.图标1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NewCheckGoal";
            this.Text = "NewCheckGoal";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button 图标1;
        private System.Windows.Forms.Button 图标2;
        private System.Windows.Forms.Button 图标3;
        private System.Windows.Forms.Button 图标4;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Button 创建打卡;
        private System.Windows.Forms.DateTimePicker startTime;
        private System.Windows.Forms.DateTimePicker endTime;
        private System.Windows.Forms.Button button1;
    }
}